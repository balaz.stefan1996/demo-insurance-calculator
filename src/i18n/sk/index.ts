export default {
  insureanceCalculator: 'Kalkulačka Poistenia',
  language: 'Jazyk',
  typeOfInsurance: 'Typ poistenia *',
  package: 'Balík *',
  additionalIssuarance: 'Máte zájem o pripoistenie ?',
  typeOfadditionalIssuarance: 'Typ pripoistenia',
  numberOfPeople: 'Počet osôb *',
  finalIssurancePriceIs: 'Cena poistenia je',
  possibleInsuranceTypeOptions: {
    shortTime: 'Kratkodobé',
    longTime: 'Celoročné',
  },
  optionInsurance: {
    basics: 'základny',
    extended: 'rozširený',
    extra: 'extra',
  },
  optionExtraInsurance: {
    noExtraInsurance: 'Nemám záujem',
    refund: 'Storno cesty',
    sport: 'Športové aktivity',
  },
  buttons: {
    continue: 'Pokračovať',
    back: 'Späť',
    showResult: 'Ukazáť výsledok',
    hideResult: 'Skryť výsledok',
  },
  dates: {
    begginingOfIsurance: 'Začiatok poistenia *',
    endOfIsurance: 'Koniec poistenia *',
    wrongOrderDates:
      'Datum konca poistenia nesmie byť skôr ako dátum začiatku poistenia',
  },
  maximumPeople: 'Maximalny počet osob 3',
  insurancePeriod: 'Doba poistenia',
  errorPage: 'Oops. Nič na zobrazenie...',
  message: {
    shortTime: 'Vybrali ste ze budete poisteny',
    longTimePartOne: ' Vybrali ste ročné poistenie od',
    longTimePartTwo: ' do konca roka 31.12.',
  },
};
