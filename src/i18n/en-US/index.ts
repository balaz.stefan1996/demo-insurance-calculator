export default {
  insureanceCalculator: 'Insureance Calculator',
  language: 'Language',
  typeOfInsurance: 'Typ of Insurance *',
  package: 'Package *',
  additionalIssuarance: 'Are you interested in additional insurance?',
  typeOfadditionalIssuarance: 'Type of additional issuarance',
  numberOfPeople: 'Number of People *',
  finalIssurancePriceIs: 'Final issurance price is',
  possibleInsuranceTypeOptions: {
    shortTime: 'Short time',
    longTime: 'Long time',
  },
  optionInsurance: {
    basics: 'basics',
    extended: 'extended',
    extra: 'extra',
  },
  optionExtraInsurance: {
    noExtraInsurance: 'I am not interested',
    refund: 'Trip cancellation',
    sport: 'Sport activities',
  },
  buttons: {
    continue: 'Continue',
    back: 'Back',
    showResult: 'Show Result',
    hideResult: 'Hide Result',
  },
  dates: {
    begginingOfIsurance: 'Start of Esurance *',
    endOfIsurance: 'End of Esurance *',
    wrongOrderDates:
      'The insurance end date must not be earlier than the insurance start date',
  },
  maximumPeople: 'Maximum number of people 3',
  insurancePeriod: 'Insurance period',
  errorPage: 'Oops. Nothing here...',
  message: {
    shortTime: 'You chose to be insured',
    longTimePartOne: ' You have selected annual insurance from',
    longTimePartTwo: 'until the end of the year 31.12.',
  },
};
