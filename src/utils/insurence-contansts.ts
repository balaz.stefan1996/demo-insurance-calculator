/**
 * Configuration object for insurance pricing.
 *
 * - `shortTimePeriod`: Contains pricing for short-term insurance packages.
 *   - `basics`: Price per day for one person with the basic package.
 *   - `extended`: Price per day for one person with the extended package.
 *   - `extra`: Price per day for one person with the extra package.
 *
 * - `longTimePeriod`: Contains pricing for long-term insurance packages.
 *   - `basics`: Price per year for one person with the basic package.
 *   - `extended`: Price per year for one person with the extended package.
 *   - `extra`: Price per year for one person with the extra package.
 *
 * - `extraShorTimeIsurance`: Contains pricing for additional insurance options.
 *   - `refund`: Multiplier for short time period to calculate the extra cost for the trip cancellation option.
 *   - `sport`: Multiplier for short time period to calculate the extra cost for the sports activities option.
 *
 *   - `extraLongTimeIsurance`: Contains pricing for additional insurance options.
 *   - `refund`: Multiplier for long time period to calculate the extra cost for the trip cancellation option.
 *   - `sport`: Multiplier for long time period to calculate the extra cost for the sports activities option.
 */

export const config = {
  shortTimePeriod: {
    basics: 1.2,
    extended: 1.8,
    extra: 2.4,
  },
  longTimePeriod: {
    basics: 39,
    extended: 49,
    extra: 59,
  },
  extraShorTimeIsurance: {
    refund: 1.5,
    sport: 1.3,
  },
  extraLongTimeIsurance:{
    refund:1.2,
    sport:1.1
  }
}
