export const validateDate = (date: string) => {
  if (!date) {
    return 'Toto pole je povinné';
  }

  const regex =
    /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\.(0[1-9]|1[0-2])\.(202[3-9]|20[3-9][0-9]|2[0-1][0-9]{2})$/;
  if (!regex.test(date)) {
    return 'Nie je zadaný validaný datum';
  }
  return true;
};
